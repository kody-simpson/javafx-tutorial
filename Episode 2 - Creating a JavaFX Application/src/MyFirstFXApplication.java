import javafx.application.Application;
import javafx.stage.Stage;

public class MyFirstFXApplication extends Application {

    //optional - method runs before start method is called
    @Override
    public void init() throws Exception {
        System.out.println("Before!!!!");
    }

    //the start method is abstract and MUST be overided when making a new javafx app
    @Override
    public void start(Stage stage) throws Exception {
        stage.setTitle("My first window!");
        stage.show();
    }

    //optional - method runs when application stops
    @Override
    public void stop() throws Exception {
        System.out.println("After!!!");
    }
}
