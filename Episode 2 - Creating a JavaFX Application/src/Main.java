import javafx.application.Application;

public class Main {

    public static void main(String[] args) {
        Application.launch(MyFirstFXApplication.class, args); //launch method is used to launch an application
    }

}
