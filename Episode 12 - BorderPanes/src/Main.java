import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class Main extends Application {


    @Override
    public void start(Stage stage) throws Exception {

        stage.setTitle("Episode 12 - BorderPane");
        stage.setWidth(500);
        stage.setHeight(500);

        //The border pane lays out nodes in 5 sections:
        //top, bottom, center, left, and right
        BorderPane root = new BorderPane();

        //Create some random controls
        Button b1 = new Button("One");
        Button b2 = new Button("Two");
        Button b3 = new Button("Three");
        Button b4 = new Button("Four");
        Button b5 = new Button("Five");

        //Set the layout of the nodes with a borderpane
        root.setCenter(b1);
        root.setTop(b2);
        root.setBottom(b3);
        root.setLeft(b4);
        root.setRight(b5);

        //Set the alignment within each border itself
        BorderPane.setAlignment(b2, Pos.CENTER);

        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

}