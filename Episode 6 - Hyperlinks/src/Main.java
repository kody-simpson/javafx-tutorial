import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Main extends Application{

    @Override
    public void start(Stage stage) throws Exception {

        stage.setTitle("Hyperlinks");
        stage.setWidth(500);
        stage.setHeight(500);
        VBox root = new VBox();

        //Hyperlinks
        Hyperlink link = new Hyperlink("Click me boi");

        //Make the link do something when clicked
        link.setOnAction(e -> System.out.println("The link was smacked!"));

//        link.setOnAction(new EventHandler<ActionEvent>() {
//            @Override
//            public void handle(ActionEvent e) {
//                System.out.println("This link is clicked");
//            }
//        });

        //another example
        Label text = new Label("Link not touched");
        ImageView image = new ImageView("https://2.gravatar.com/avatar/ba9ac7f7a62a76857490f8ebd1d17a03?s=40&d=https%3A%2F%2F2.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fs%3D40&r=PG");
        Hyperlink link2 = new Hyperlink("Smack Me!", image);
        link2.setOnAction(e -> {
            text.setText("Link used!");
        });

        root.getChildren().addAll(link, link2, text);
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
