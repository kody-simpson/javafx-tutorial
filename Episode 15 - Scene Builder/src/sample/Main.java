package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("resources/sample.fxml"));
        //Create a scene for each fxml file after loading it
        Parent loginformRoot = FXMLLoader.load(getClass().getResource("resources/loginform.fxml"));
        Scene loginform = new Scene(loginformRoot);

        primaryStage.setTitle("Hello World");
        primaryStage.setScene(loginform);
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
