import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        stage.setTitle("Episode 11 - HBox and VBox");
        stage.setWidth(400);
        stage.setHeight(500);

        HBox root = new HBox(); //Every child node is laid out on top of each other
        root.setSpacing(20); //space between each node
        //root.setPadding(new Insets(20, 40, 20, 40));
        root.setAlignment(Pos.BOTTOM_LEFT); //Mess with the alignment of all the nodes in the hbox

        Button b1 = new Button("One");
        Button b2 = new Button("Two");
        Button b3 = new Button("Three");
        Button b4 = new Button("Four");

        root.setMargin(b1, new Insets(10, 10, 10, 10)); //Set padding for an individual node

        //Everything is generally the same for the Vbox

        root.getChildren().addAll(b1, b2, b3, b4);
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }
}
