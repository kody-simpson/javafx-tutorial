import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        stage.setTitle("Title for window");

        VBox root = new VBox();

        //Add controls(child nodes) to the parent node

        //Displaying Images with ImageView
        ImageView imageView = new ImageView("https://cdn.arstechnica.net/wp-content/uploads/2016/02/5718897981_10faa45ac3_b-640x624.jpg");
        Image image = new Image("https://static.interestingengineering.com/images/APRIL/sizes/black_hole_resize_md.jpg");
        //MAKE SURE THE IMAGE OBJECT IMPORT IS FROM JAVAFX
        ImageView imageView2 = new ImageView(image); //or provide an image object

        //Labels - Display text
        Label label1 = new Label("This is a text label"); //make a basic label
        //Label with an image
        Label labelWithImage = new Label("Cool Image: ", imageView);
        label1.setTextFill(Color.web("#5042f4")); //customize the text color
        //fonts
        label1.setFont(new Font("Cambria", 45)); //font name first, size second
        //transform it
        label1.setRotate(34);

        root.getChildren().addAll(label1);
        Scene scene = new Scene(root);
        stage.setScene(scene);

        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
