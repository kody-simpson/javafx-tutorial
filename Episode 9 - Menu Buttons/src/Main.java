import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Main extends Application{


    @Override
    public void start(Stage stage) throws Exception {

        stage.setTitle("Episode 9 - Menu Buttons");
        stage.setWidth(400);
        stage.setHeight(500);

        VBox root = new VBox();

        //Menu Buttons
        //First, create the menu options
        MenuItem item1 = new MenuItem("Taco");
        MenuItem item2 = new MenuItem("Burrito");
        MenuItem item3 = new MenuItem("Cheese Enchilada");
        MenuItem item4 = new MenuItem("Chips & Salsa");

        //Now, create the main button to hold these options
        MenuButton menuButton = new MenuButton("Favorite Mexican Food", null, item1, item2, item3, item4);
        Label food = new Label("No Food Selected");

        //Add Events for when an option is selected
        item1.setOnAction(e -> {
            food.setText("Food: Taco");
        });
        item2.setOnAction(e -> {
            food.setText("Food: Burrito");
        });
        item3.setOnAction(e -> {
            food.setText("Food: Cheese Enchilada");
        });
        item4.setOnAction(e -> {
            food.setText("Food: Chips & Salsa");
        });

        root.getChildren().addAll(food, menuButton);
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();

    }

    public static void main(String[] args) {
        launch(args);
    }

}