import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage stage) throws Exception {

        stage.setTitle("Episode 14 - GridPanes (w/ TextField & PasswordField)");
        //stage.setWidth(250);
        //stage.setHeight(250);

        Label info = new Label("Create a new account here by entering your info");

        Label usernameLabel = new Label("Username:");
        Label emailLabel = new Label("Email:");
        Label passwordLabel = new Label("Password:");

        TextField usernameField = new TextField();
        TextField emailField = new TextField();
        PasswordField passwordField = new PasswordField();

        Button finishButton = new Button("Create Account");

        GridPane root = new GridPane();

        //Customize the gridpane
        //root.setGridLinesVisible(true);
        root.setVgap(10);
        root.setHgap(10);
        root.setPadding(new Insets(50));

        //column, row
        root.add(usernameLabel, 0, 0);
        root.add(usernameField, 1, 0);
        root.add(emailLabel, 0, 1);
        root.add(emailField, 1, 1);
        root.add(passwordLabel, 0, 2);
        root.add(passwordField, 1, 2);
        root.add(finishButton, 1, 3);

        root.add(info,1, 4);
        Button test = new Button("Testing");
        root.add(test, 2, 4);

        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

}
