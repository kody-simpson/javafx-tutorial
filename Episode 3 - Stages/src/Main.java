import javafx.application.Application;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class Main extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        stage.setTitle("Episode 3");
        stage.show();

        //Create a new window
        Stage newWindow = new Stage();
        newWindow.setTitle("A cool window");


        Stage stage3 = new Stage();
        stage3.setTitle("Third window");
        stage3.show();
        //Change the position of the window
        newWindow.setX(50);
        newWindow.setY(100);
        //Set the size of the window
        newWindow.setWidth(500);
        newWindow.setHeight(900);

        //Other windows cant be used until this one is closed
        newWindow.initModality(Modality.APPLICATION_MODAL);

        //The owner stage cant be used until the daughter stage is closed
//        newWindow.initOwner(stage3);
//        newWindow.initModality(Modality.WINDOW_MODAL);

        //Style the window
        newWindow.initStyle(StageStyle.DECORATED);

        //newWindow.initStyle(StageStyle.UNDECORATED);
        //newWindow.initStyle(StageStyle.TRANSPARENT);
        //newWindow.initStyle(StageStyle.UNIFIED);
        //newWindow.initStyle(StageStyle.UTILITY);

        //You have to style the stage BEFORE showing it
        newWindow.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

}
