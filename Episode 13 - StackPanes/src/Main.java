import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class Main extends Application{

    @Override
    public void start(Stage stage) throws Exception {

        stage.setTitle("Episode 13 - StackPane");
//        stage.setWidth(500);
//        stage.setHeight(500);

        //Why use the stackpane? To stack things :)

//        Label l1 = new Label("Im a really cool label");
        Button b2 = new Button("Smack Elon Musk");

        ImageView image = new ImageView("https://cdn.discordapp.com/attachments/288494706697043969/608820574579327005/IMG_20190806_225622.jpg");

        StackPane root = new StackPane(image, b2);

        //Set the position of a node on the stack
        StackPane.setAlignment(b2, Pos.BOTTOM_LEFT);

        //Set the margin of a node on the stack
        StackPane.setMargin(b2, new Insets(30));

        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

}