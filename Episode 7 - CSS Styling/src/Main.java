import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        stage.setTitle("Episode 7 - CSS Styling");
        stage.setWidth(400);
        stage.setHeight(500);

        VBox root = new VBox();
        //stylesheets for parent nodes
        root.getStylesheets().add("stylesheets/fire.css");

        Label label1 = new Label("This is a cool label bro.");
        Label label2 = new Label("This is a corfewrewrol label bro.");
        label2.setId("special-label");

        Hyperlink link = new Hyperlink("qweqweqwe");
        //styling for individual controls
        link.setStyle("-fx-background-color: lightcoral; -fx-rotate: 45");

        root.getChildren().addAll(label1, label2, link);

        Scene scene = new Scene(root);
        //Scene specific stylesheets
        scene.getStylesheets().add("stylesheets/styles.css");
        stage.setScene(scene);
        stage.show();

    }

    public static void main(String[] args) {
        launch(args);
    }

}
