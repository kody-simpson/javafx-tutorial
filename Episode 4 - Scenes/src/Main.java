import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage stage) throws Exception {

        stage.setTitle("Episode 4 - Scenes");
        stage.setWidth(400);
        stage.setHeight(500);

        //To display things, there needs to be a scene
        VBox parent = new VBox(); //parent because all the child nodes will go inside

        Label label1 = new Label("This is a text label"); //Create a child node
        Label label2 = new Label("This is another label"); //Create another
        parent.getChildren().addAll(label1, label2); //Add child nodes to the parent node

        Scene scene1 = new Scene(parent); //Create a scene using a root/parent node
        //add the scene to the stage
        stage.setScene(scene1); //Only one scene can be displayed at a time

        stage.show();

    }
}
