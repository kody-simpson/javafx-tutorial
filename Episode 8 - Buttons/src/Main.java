import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        stage.setTitle("Episode 8 - Buttons");
        stage.setWidth(500);
        stage.setHeight(500);
        VBox root = new VBox();

        //Buttons
        Button button1 = new Button("Click Me");
        //button1.setStyle("-fx-font-size: 45px");
        //Text wrapping - for when the button text is too long
        button1.setText("_Republicans are really cool");
        button1.setWrapText(true);

        //Sizing
        button1.setMinSize(50, 50); //set the minimum size the button can be
        button1.setPrefSize(100, 100);//set the preferred size a button can be
        button1.setMaxSize(150, 150); //set the max size the button can be

        //Mnenomic - Shortcut for buttons
        button1.setMnemonicParsing(true); //Button can be be selected and pressed with ALT R

        //We will test to see if it worked with an event
        button1.setOnAction(e -> {
            System.out.println("Button tapped");
        });

        root.getChildren().addAll(button1);
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

}
